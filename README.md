# Shannon-Fano-Elias

Кодот се состои од две класи: Value и ShannonFanoElias.

Класата Value претставува пар (x, p(x)) и е прилично едноставна. Составена е само од конструктор.

Класата ShannonFanoElias содржи повеќе работи за крајна функционалност на кодирањето.

Покрај конструкторот ги има следните функции:
Fx - прима аргумент листа од Values, односно една распределба,  пресметува F(x) почнувајќи од нула и додавајќи ги соодветно p(x), како резултат враќа листа од F(x) вредности

F1x - прима распределба и листата која ја враќа како резултат функцијата Fx,  пресметува F1(x) според формулата F1(x) = F(x) + (p(x)  / 2), како резултат враќа листа од F1(x) вредности

float_bin, decimal_convertor, F1_binary - функции за конвертирање на float вредности во бинарни вредности

lx - прима распределба и пресметува должина на коден збор според формулата l(x) = ceil(−log2 p(x)) + 1, на излез враќа листа од должини

koden_zbor - функција која од бинарните вредности, зема коден збор со должина l(x), на излез враќа листа од кодни зборови

Во main класата постојат проверки во однос на тоа дали за p(x) е внесена вредност помеѓу 0 и 1 (доколку не е враќа порака за грешка) и проверка на сумата од сите веројатности, доколку е различна од 1, не станува збор за распределба и враќа соодветна порака за грешка.
Освен проверките, има и табела за сликовит приказ на резултатот. Останатото е само внесување на вредностите преку стандарден влез и повикување на функциите од останатите класи.
