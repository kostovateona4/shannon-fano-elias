import math
import plotly.graph_objects as go


class Value:
    def __init__(self, x, px):
        self.x = x
        self.px = px


class ShannonFanoElias:
    def __init__(self, values):
        self.values = values

    @staticmethod
    def Fx(lista):
        rezultat = []
        sum = 0
        rezultat.append(sum)

        for i in lista:
            sum += i.px
            rezultat.append(sum)

        return rezultat

    @staticmethod
    def F1x(lista, fx):
        n = len(lista)
        rezultat = []
        sum = 0

        for i in range(n):
            sum = fx[i] + (raspredelba[i].px / 2)
            rezultat.append(sum)

        return rezultat

    @staticmethod
    def float_bin(number, places):
        whole, dec = str(number).split(".")
        whole = int(whole)
        dec = int(dec)
        res = bin(whole).lstrip("0b") + "."

        for x in range(places):
            tmp = str((ShannonFanoElias.decimal_converter(dec) * 2)).split(".")
            whole, dec = tmp
            dec = int(dec)
            res += whole

        return res

    @staticmethod
    def decimal_converter(num):
        while num > 1:
            num /= 10
        return num * 1.0

    @staticmethod
    def F1_binary(f1x, lx):
        rezultat = []

        for i in range(len(f1x)):
            binaren = ShannonFanoElias.float_bin(f1x[i], lx[i])
            rezultat.append(binaren)

        return rezultat

    @staticmethod
    def lx(lista):
        rezultat = []

        for i in lista:
            tmp = -math.log(i.px, 2)
            tmp = math.ceil(tmp)
            tmp += 1
            rezultat.append(tmp)

        return rezultat

    @staticmethod
    def koden_zbor(binarna_reprezentacija):
        rezultat = []

        for i in binarna_reprezentacija:
            tmp = i.split(".")
            rezultat.append(tmp[1])

        return rezultat


if __name__ == '__main__':
    raspredelba = []
    while True:
        x = input()
        if x == "KRAJ":
            break

        px = float(input())
        if px < 0 or px > 1:
            print("Vnesena e nevalidna vrednost za p(x)")

        else:
            value = Value(x, px)
            raspredelba.append(value)

    sum = 0
    for i in raspredelba:
        sum += i.px

    if sum != 1:
        print("Vnesenite vrednosti ne pretstavuvaat raspredelba")

    else:
        vrednost = []
        verojatnosti = []
        for i in raspredelba:
            vrednost.append(i.x)
            verojatnosti.append(i.px)

        fx = ShannonFanoElias.Fx(raspredelba)
        f1x = ShannonFanoElias.F1x(raspredelba, fx)
        lx = ShannonFanoElias.lx(raspredelba)
        binarno = ShannonFanoElias.F1_binary(f1x, lx)
        koden_zbor = ShannonFanoElias.koden_zbor(binarno)

        fig = go.Figure(data=[
            go.Table(header=dict(values=['x', 'p(x)', 'F(x)', 'F1(x)', 'F1(x) бинарно', 'l(x)', 'к.з']),
                     cells=dict(values=[vrednost, verojatnosti, fx, f1x, binarno, lx, koden_zbor]))])
        fig.show()
